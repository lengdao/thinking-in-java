package container.deep;

import utils.Generator;

/**
 * @author Mr.Sun
 * @date 2022年08月28日 9:58
 */
public class Government implements Generator<String> {
    String[] foundation = ("strange women lying in ponds " +
            "distributing swords is no basis for a system of " +
            "government").split(" ");

    private int index;
    @Override
    public String next() {
        return foundation[index++];
    }
}
