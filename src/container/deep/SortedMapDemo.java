package container.deep;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * @author Mr.Sun
 * @date 2022年08月29日 10:15
 *
 * 使用SortedMap（TreeMap是其现阶段的唯一实现），可以确保键处于排序状态，这使得它具有额外的功能，
 * 这些功能由SortedMap接口中的下列方法提供：
 * Comparator comparator(): 返回当前Map使用的Comparator；或者返回null，表示以自然方式排序
 * T firstKey()返回Map中的第一个键
 * T lastKey()返回Map中的最末一个键
 * SortedMap subMap(fromKey, toKey)生成此Map的子集，范围由formKey(包含)到toKey(不包含)的键确定
 * SortedMap headMap(toKey)生成此Map的子集，由键小于toKey的所有键值对组成。
 * SortedMap tailMap(fromKey)生成此Map的子集，由键大于或等于formKey的所有键值对组成。
 */
public class SortedMapDemo {

    /**
     * 演示 TreeMap 新增的功能
     */
    public static void main(String[] args) {
        TreeMap<Integer, String> sortedMap = new TreeMap<>(
                new CountingMapData(10));
        System.out.println(sortedMap);

        Integer firstKey = sortedMap.firstKey();
        System.out.println(firstKey);

        Integer lastKey = sortedMap.lastKey();
        System.out.println(lastKey);

        Iterator<Integer> iterator = sortedMap.keySet().iterator();
        for (int i = 0; i <= 6; i++) {
            if (i == 3) {
                firstKey = iterator.next();
            }
            if (i == 6) {
                lastKey = iterator.next();
            } else {
                iterator.next();
            }
        }

        System.out.println(firstKey);
        System.out.println(lastKey);
        System.out.println(sortedMap.subMap(firstKey, lastKey));
        System.out.println(sortedMap.headMap(lastKey));
        System.out.println(sortedMap.tailMap(firstKey));
    }
}
