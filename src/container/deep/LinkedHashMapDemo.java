package container.deep;

import java.util.LinkedHashMap;

/**
 * @author Mr.Sun
 * @date 2022年08月28日 17:43
 *
 * LinkedHashMap:为了提高速度，LinkedHashMap散列化所有元素，但是在遍历键值对时，却又以键值对的插入顺序返回键值对。
 * 此外，可以在构造器中设定LinkedHashMap，使之采用最近最少用（LRU）算法，于是没有被访问过的（可看作需要被删除的）元素
 * 就会出现在队列的前面。对于需要定期清理元素以节省空间的程序的来说，此功能使得程序容易实现。
 */
public class LinkedHashMapDemo {

    public static void main(String[] args) {
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>(
                new CountingMapData(9)
        );
        System.out.println(linkedHashMap);

        // LRU顺序
        linkedHashMap = new LinkedHashMap<>(16, 0.75f, true);
        linkedHashMap.putAll(new CountingMapData(9));
        System.out.println(linkedHashMap);

        for (int i = 0; i < 6; i++) {
            linkedHashMap.get(i);
        }
        System.out.println(linkedHashMap);

        linkedHashMap.get(0);
        System.out.println(linkedHashMap);
    }

}
