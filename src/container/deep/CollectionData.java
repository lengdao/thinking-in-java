package container.deep;

import utils.Generator;

import java.util.ArrayList;

/**
 * @author Mr.Sun
 * @date 2022年08月28日 9:42
 *
 * 适配器设计模式实例：该类是为了更容易的创建测试数据
 */
public class CollectionData<T> extends ArrayList<T> {
    // 使用Generator在容器中放置所需数量的对象
    public CollectionData(Generator<T> gen, int quantity) {
        for (int i = 0; i < quantity; i++) {
            add(gen.next());
        }
    }

    //一种通用的方便方法
    public static <T> CollectionData<T> list(Generator<T> gen, int quantity) {
        return new CollectionData<>(gen, quantity);
    }
}
