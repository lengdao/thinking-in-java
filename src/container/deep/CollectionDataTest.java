package container.deep;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Mr.Sun
 * @date 2022年08月28日 10:00
 */
public class CollectionDataTest {

    public static void main(String[] args) {
        Set<String> set = new LinkedHashSet<>(
                new CollectionData<>(new Government(), 15)
        );
        set.addAll(CollectionData.list(new Government(), 15));
        System.out.println(set);
    }
}
