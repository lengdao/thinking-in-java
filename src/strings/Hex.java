package strings;

/**
 * @author Mr.Sun
 * @date 2022年02月16日 21:05
 *
 * 一个十六进制转储工具
 */
public class Hex {

    public static String format(byte[] data) {
        StringBuilder result = new StringBuilder();
        int n = 0;
        for (byte b : data) {
            if (n % 16 == 0) {
                result.append(String.format("%05X: ", n));
            }
            result.append(String.format("%02X ", b));
            n++;
            if (n % 16 == 0) {
                result.append("\n");
            }
        }
        result.append("\n");
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(format("Reminis".getBytes()));
    }
}
