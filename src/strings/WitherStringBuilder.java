package strings;

/**
 * @author Mr.Sun
 * @date 2022年04月10日 20:44
 *
 * 测试分别使用String和StringBuilder进行字符串拼接
 */
public class WitherStringBuilder {

    /**
     * 使用String进行字符串拼接
     * @param fields 字符串数组
     * @return 拼接后的字符串
     */
    public String implicit(String[] fields) {
        String result = "";
        for (int i = 0; i < fields.length; i++) {
            result += fields[i];
        }
        return result;
    }

    /**
     * 使用StringBuilder进行字符串拼接
     * @param fields 字符串数组
     * @return 拼接后的字符串
     */
    public String explicit(String[] fields) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            result.append(fields[i]);
        }
        return result.toString();
    }
}
