package strings;

/**
 * @author Mr.Sun
 * @date 2022年02月16日 21:02
 *
 * String.format()
 */
public class DatabaseException extends Exception {
    public DatabaseException(int transactionId, int queryId, String message) {
        super(String.format("(t%d, q%d) %s",transactionId, queryId, message));
    }

    public static void main(String[] args) {
        try {
            throw new DatabaseException(3, 7, "Write failed");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
