package io;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Mr.Sun
 * @date 2022年08月30日 20:59
 *
 * 可以添加新行，并缩排所有元素的工具
 */
public class PPrint {

    public static String pformat(Collection<?> collection) {
        if (collection.size() == 0) {
            return "[]";
        }

        StringBuilder result = new StringBuilder("[");
        for (Object item : collection) {
            if (collection.size() != 1) {
                result.append("\n  ");
            }
            result.append(item);
        }
        if (collection.size() != 1) {
            result.append("\n");
        }
        result.append("]");
        return result.toString();
    }

    public static void pprint(Collection<?> collection) {
        System.out.println(pformat(collection));
    }

    public static void pprint(Object[] c) {
        System.out.println(pformat(Arrays.asList(c)));
    }
}
