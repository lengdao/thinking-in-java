package io;

import java.io.File;

/**
 * @author Mr.Sun
 * @date 2022年08月30日 21:24
 *
 * 目录的检查及创建
 */
public class MakeDirectories {
    private static void usage() {
        System.err.println(
                "Usage:MakeDirectories path1 ...\n" +
                        "Creates each path\n" +
                        "Usage:MakeDirectories -d path1 ...\n" +
                        "Deletes each path\n" +
                        "Usage:MakeDirectories -r path1 path2\n" +
                        "Renames from path1 to path2");
        System.exit(1);
    }

    private static void fileData(File f) {
        System.out.println(
                "绝对路径: " + f.getAbsolutePath() +
                        "\n 可读: " + f.canRead() +
                        "\n 可写: " + f.canWrite() +
                        "\n 文件名称: " + f.getName() +
                        "\n 上级目录: " + f.getParent() +
                        "\n 文件路径: " + f.getPath() +
                        "\n 文件大小: " + f.length() +
                        "\n 最后修改时间: " + f.lastModified());
        if(f.isFile()) {
            System.out.println("这是一个文件");
        } else if(f.isDirectory()) {
            System.out.println("这是一个目录");
        }
    }

    public static void main(String[] args) {
        if(args.length < 1) {
            usage();
        }

        if(args[0].equals("-r")) {
            if(args.length != 3) {
                usage();
            }

            File old = new File(args[1]), rname = new File(args[2]);
            old.renameTo(rname);
            fileData(old);
            fileData(rname);
            return; // Exit main
        }

        int count = 0;
        boolean del = false;
        if(args[0].equals("-d")) {
            count++;
            del = true;
        }
        count--;
        while(++count < args.length) {
            File f = new File(args[count]);
            if(f.exists()) {
                System.out.println(f + " exists");
                if(del) {
                    System.out.println("deleting..." + f);
                    f.delete();
                }
            }
            else { // Doesn't exist
                if(!del) {
                    f.mkdirs();
                    System.out.println("created " + f);
                }
            }
            fileData(f);
        }
    }
}
