package io;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Objects;

/**
 * @author Mr.Sun
 * @date 2022年08月31日 21:11
 *
 * 格式化的内存输入
 */
public class FormatMemoryInput {

    public static void main(String[] args) throws IOException {
        DataInputStream in = null;
        try {
            in = new DataInputStream(new ByteArrayInputStream(
                    BufferInputFile.read("src/io/FormatMemoryInput.java").getBytes()));

            while (true) {
                System.out.print((char) in.readByte());
            }
        } catch (EOFException e) {
            System.err.println("End of stream");
        } finally {
            if (Objects.nonNull(in)) {
                in.close();
            }
        }
    }
}
