package io;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * @author Mr.Sun
 * @date 2022年08月31日 21:18
 *
 * 演示如何一次一个字节地读取文件
 */
public class TestEOFE {

    public static void main(String[] args) throws IOException {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(
                BufferInputFile.read("src/io/TestEOFE.java").getBytes()));

        while (in.available() != 0) {
            System.out.print((char) in.readByte());
        }
    }
}
