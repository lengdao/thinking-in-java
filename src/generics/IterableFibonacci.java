package generics;

import java.util.Iterator;

/**
 * @author Mr.Sun
 * @date 2022年02月22日 20:47
 *
 * 如果要在循环语句中使用IterableFibonacci,必须向IterableFibonacci的构造器
 * 提供一个边界值，然后hasNext()方法才能知道如何返回false
 */
public class IterableFibonacci  implements Iterable<Integer> {
    private int n;
    private Fibonacci fib;

    public IterableFibonacci(int n) {
        this.n = n;
        fib = new Fibonacci();
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return n > 0;
            }

            @Override
            public Integer next() {
                n--;
                // 使用.this语法，生成外部类对象的引用，然后调用外部类对象的方法
                return fib.next();
            }
        };
    }

    public static void main(String[] args) {
        for (Integer i : new IterableFibonacci(18)) {
            System.out.print(i + " ");
        }
    }
}
