package generics;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Mr.Sun
 * @date 2022年02月21日 21:32
 */
public class RandomList<T> {
    // 持有特定类型对象的列表
    private ArrayList<T> storage = new ArrayList<T>();
    private Random rand = new Random(47);
    public void add(T item) { storage.add(item); }

    // 通过该方法，可以随机选取一个元素
    public T select() {
        return storage.get(rand.nextInt(storage.size()));
    }

    public static void main(String[] args) {
        RandomList<String> rs = new RandomList<String>();
        for(String s: ("The quick brown fox jumped over " +
                "the lazy brown dog").split(" "))
            rs.add(s);
        for(int i = 0; i < 11; i++)
            System.out.print(rs.select() + " ");
    }
}
