package generics;

import java.util.*;

/**
 * @author Mr.Sun
 * @date 2022年02月23日 21:05
 *
 * 获取有泛型声明所声明的类型参数
 */
class Frob {}
class Fnorkle {}
class Quark<Q> {}
class Particle<POSITION,MOMENTUM> {}

public class LostInformation {
    public static void main(String[] args) {
        // 只能获取到用作参数占位符的标识符
        // 在泛型代码内部，无法获取到任何有关泛型参数类型的信息
        List<Frob> list = new ArrayList<>();
        System.out.println(Arrays.toString(list.getClass().getTypeParameters()));

        Map<Frob,Fnorkle> map = new HashMap<>();
        System.out.println(Arrays.toString(map.getClass().getTypeParameters()));

        Quark<Fnorkle> quark = new Quark<>();
        System.out.println(Arrays.toString(quark.getClass().getTypeParameters()));

        Particle<Long,Double> p = new Particle<>();
        System.out.println(Arrays.toString(p.getClass().getTypeParameters()));
    }
}
