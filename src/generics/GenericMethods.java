package generics;

/**
 * @author Mr.Sun
 * @date 2022年02月22日 20:54
 *
 * 泛型方法
 */
public class GenericMethods {

    // 泛型方法
    // <T> 叫做类型参数
    public <T> void f(T t) {
        System.out.println(t.getClass().getSimpleName());
    }

    public static void main(String[] args) {
        GenericMethods gen = new GenericMethods();
        gen.f("");
        gen.f(1);
        gen.f(1.0);
        gen.f(1.0F);
        gen.f('c');
        gen.f(gen);
    }

}
