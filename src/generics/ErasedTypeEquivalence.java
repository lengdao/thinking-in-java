package generics;

import java.util.ArrayList;

/**
 * @author Mr.Sun
 * @date 2022年02月23日 21:03
 *
 * 泛型擦除
 */
public class ErasedTypeEquivalence {

    public static void main(String[] args) {
        Class c1 = new ArrayList<String>().getClass();
        Class c2 = new ArrayList<Integer>().getClass();
        System.out.println(c1 == c2);
    }
}
