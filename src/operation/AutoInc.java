package operation;

/**
 * @author Mr.Sun
 * @date 2022年01月16日 18:32
 * 自增和自减操作符测试
 */
public class AutoInc {

    public static void main(String[] args) {
        int i = 1;
        System.out.println("i: " + i); // 1
        System.out.println("++i: " + ++i); // 执行完运算后才得到值，故输出2
        System.out.println("i++: " + i++); // 运算执行之前就得到值，故输出2
        System.out.println("i: " + i); //  3
        System.out.println("--i: " + --i); // 执行完运算后才得到值，故输出2
        System.out.println("i--: " + i--); // 运算执行之前就得到值，故输出2
        System.out.println("i: " + i); // 1
    }
}
