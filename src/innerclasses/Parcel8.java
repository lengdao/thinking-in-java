package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 11:13
 *
 * 匿名内部类：将返回值的生成与表示这个返回值的类的定义结合在一起，此外这个类没有名字。
 */
public class Parcel8 {

    public Contents contents() {
        return new Contents() {
            private int i = 11;
            @Override
            public int value() {
                return i;
            }
        }; // 这个分号是必须的
    }

    public static void main(String[] args) {
        Parcel8 parcel8 = new Parcel8();
        Contents c = parcel8.contents();
        System.out.println(c.value());
    }
}
