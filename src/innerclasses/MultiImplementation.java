package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月23日 10:36
 *
 * 使用内部类解决多重基层问题
 */
class D {}
abstract class E {}
class Z extends D {
    E makeE() {
        return new E() {};
    }
}

public class MultiImplementation {
    static void taskD(D d) {};
    static void taskE(E e) {};

    public static void main(String[] args) {
        Z z = new Z();
        taskD(z);
        taskE(z.makeE());
    }
}
