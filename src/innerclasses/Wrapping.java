package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 11:23
 */
public class Wrapping {

    private int i;
    public Wrapping(int x) {
        i = x;
    }

    public int value() {
        return i;
    }
}
