package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月23日 9:50
 */
public class Parcel10 {
    // 如果定义一个匿名内部类，并且希望它使用一个在其外部定义的对象，那么编译器会要求
    // 其参数是final的，如果你忘记写了，这个参数也是默认为final的
    public Destination destination(final String dest) {
        return new Destination() {
            // 简单地给一个字段赋值
            private String label = dest;
            @Override
            public String readLabel() {
                return label;
            }
        };
    }

    public static void main(String[] args) {
        Parcel10 p = new Parcel10();
        Destination d = p.destination("Tasmania");
    }

}
