package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 10:17
 *
 * 如果你需要生成对外部类对象的引用，可以使用外部类的名字后面紧跟原点和this
 */
public class DoThis {
    void f() {
        System.out.println("DoThis.f()");
    }

    public class Inner {
        public DoThis outer() {
            return DoThis.this;
        }
    }

    public Inner inner(){
        return new Inner();
    }

    public static void main(String[] args) {
        DoThis dt = new DoThis();
        Inner inner = dt.inner();
        inner.outer().f();
    }
}
