package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月23日 10:05
 */
abstract class Base {
    public Base(int i) {
        System.out.println("Base Constructor, i = " + i);
    }
    public abstract void f();
}

public class AnonymousConstructor {
    public static Base getBase(int i) {
        return new Base(i) {
            // 实例初始化的效果类似于构造器
            {
                System.out.println("Inside instance initializer");
            }
            @Override
            public void f() {
                System.out.println("In anonymous f()");
            }
        };
    }

    public static void main(String[] args) {
        Base base = getBase(47);
        base.f();
    }

}

