package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 11:25
 */
public class Parcel9 {
    public Wrapping wrapping(int x) {
        return new Wrapping(x) {
            public int value() {
                return super.value() * 47;
            }
        };
    }

    public static void main(String[] args) {
        Parcel9 p = new Parcel9();
        Wrapping w = p.wrapping(10);
        System.out.println(w.value());
    }
}
