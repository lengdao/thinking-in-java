package concurrency;

import java.util.concurrent.TimeUnit;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 16:47
 *
 * <p>
 * 后台线程：是指在程序运行的时候在后台提供的一种通用服务的线程，并且这种线程并不属于程序中不可或缺的部分。
 * 因此，所有的非后台线程结束时，程序也就终止了，同时会杀死进程中所有的后台线程。反过来说，只要有任何非后台
 * 线程还在运行，程序就不会终止。比如，执行main()的就是一个非后台线程
 * </p>
 */
public class SimpleDaemons implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            Thread daemon = new Thread(new SimpleDaemons());
            // 必须在调用start()方法之前设置
            daemon.setDaemon(true);
            daemon.start();
        }
        System.out.println("all daemon started");
        TimeUnit.MILLISECONDS.sleep(175);
    }

    @Override
    public void run() {
        try {
            while (true) {
                TimeUnit.MILLISECONDS.sleep(100);
                System.out.println(Thread.currentThread() + " " + this);
            }
        } catch (InterruptedException e) {
            System.out.println("sleep() Interrupted");
        }

    }
}
