package concurrency;

/**
 * @author Mr.Sun
 * @date 2022年09月04日 10:49
 *
 * 同步控制EventGenerator
 */
public class SyncEventGenerator extends IntGenerator {
    private int currentCountVal = 0;

    @Override
    public synchronized int next() {
        ++currentCountVal;
        Thread.yield();
        ++currentCountVal;
        return currentCountVal;
    }

    public static void main(String[] args) {
        EventChecker.test(new SyncEventGenerator());
    }
}
