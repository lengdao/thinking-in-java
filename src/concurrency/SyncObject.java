package concurrency;

import java.util.Objects;

/**
 * @author Mr.Sun
 * @date 2022年09月04日 16:19
 *
 * 在其它对象上同步
 */
public class SyncObject {
    public static void main(String[] args) {
        final DualSync ds = new DualSync();

        new Thread() {
            @Override
            public void run() {
                ds.f();
            }
        }.start();
        ds.g();
    }
}

class DualSync {
    private Object obj = new Object();

    public synchronized void f() {
        for (int i = 0; i < 5; i++) {
            System.out.println("f()");
            Thread.yield();
        }
    }

    public void g() {
        synchronized (obj) {
            for (int i = 0; i < 5; i++) {
                System.out.println("g()");
                Thread.yield();
            }
        }
    }
}

