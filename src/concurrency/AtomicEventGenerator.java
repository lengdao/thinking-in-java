package concurrency;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Mr.Sun
 * @date 2022年09月04日 15:17
 *
 * 使用AtomicInteger来重写MutexEventGenerator
 */
public class AtomicEventGenerator extends IntGenerator {
    private AtomicInteger currentEventVal = new AtomicInteger(0);

    @Override
    public int next() {
        return currentEventVal.addAndGet(2);
    }

    public static void main(String[] args) {
        EventChecker.test(new AtomicEventGenerator());
    }
}
