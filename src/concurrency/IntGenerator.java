package concurrency;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 21:49
 *
 * <p>
 *     创建一个名为IntGenerator的抽象类，它包含EventChecker必须了解的必不可少的方法，即一个next()方法，和一个可以执行撤销的方法
 * </p>
 */
public abstract class IntGenerator {

    private volatile boolean canceled = false;
    public abstract int next();

    /**
     * 修改canceled标志的状态
     */
    public void cancel() {
        canceled = true;
    }

    /**
     * 查看该对象是否已被撤销
     */
    public boolean isCanceled() {
        return canceled;
    }

}
