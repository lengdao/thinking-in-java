package concurrency;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 10:14
 *
 * 演示通过实现Runnable接口定义任务
 *
 * <p>
 *     显示发射之前的倒计时
 * </p>
 */
public class LiftOff implements Runnable {
    protected int countDown = 10;
    private static int taskCount = 0;
    private final int id = taskCount++;

    public LiftOff() {
    }

    public LiftOff(int countDown) {
        this.countDown = countDown;
    }

    public String status() {
        return "#" + id + "(" + (countDown > 0 ? countDown : "LiftOff!") + "). ";
    }

    @Override
    public void run() {
        while (countDown-- > 0) {
            System.out.print(status());
            Thread.yield();
        }
    }

} ///:~
