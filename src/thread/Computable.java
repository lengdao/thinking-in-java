package thread;

/**
 * @author Mr.Sun
 * @date 2022年03月09日 21:58
 */
public interface Computable<A, V> {

    V compute(A arg) throws InterruptedException;
}
