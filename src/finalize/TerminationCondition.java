package finalize;

/**
 * @author Mr.Sun
 * @date 2022年01月16日 19:41
 *
 * 终结条件
 * 示范finalize()可能的使用方式
 */
public class TerminationCondition {
    public static void main(String[] args) {
        Book novel = new Book(true);
        // proper cleanup
        novel.checkIn();
        // Drop the reference, forget to clean up
        new Book(true);
        // Force garbage collection & finalization
        System.gc();
    }
}

class Book {
    boolean checkOut = false;
    Book(boolean checkOut) {
        this.checkOut = checkOut;
    }
    void checkIn() {
        checkOut = false;
    }
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (checkOut) {
            System.out.println("Error: checked out");
        }
    }
}
