package interfaces.interfaceprocessor;

import interfaces.filters.*;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 11:07
 */
public class FilterProcessor {
    public static void main(String[] args) {
        Waveform w = new Waveform();
        Apply.process(new FilterAdapter(new LowPass(1.0)), w);
        Apply.process(new FilterAdapter(new HighPass(2.0)), w);
        Apply.process(new FilterAdapter(new BandPass(3.0, 4.0)), w);
    }
}

/**
 * 有时候，我们会碰到我们无法修改我们想要使用的类，这是我们就可以使用适配器设计模式
 *  适配器中的代码将接受你所拥有的接口，并产生你所需要的接口，如下：
 */
class FilterAdapter implements Processor {

    Filter filter;

    public FilterAdapter(Filter filter) {
        this.filter = filter;
    }

    @Override
    public String name() {
        return filter.name();
    }

    @Override
    public Waveform process(Object input) {
        return filter.process((Waveform) input);
    }
}
