package interfaces.interfaceprocessor;

/**
 * Processor和Apply的修改版
 */
public interface Processor {

    String name();
    Object process(Object input);
}
