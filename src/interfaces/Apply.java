package interfaces;

import java.util.Arrays;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:32
 *
 * 创建一个能够根据所传递的参数对象的不同，而具有不同行为的方法，被称为策略设计模式
 * 策略就是传递进去的参数对象，它包含要执行的代码
 */
public class Apply {
    public static void process(Processor processor, Object s) {
        System.out.println("Using Processor " + processor.name());
        System.out.println(processor.process(s));
    }

    public static String s = "Disagreement with beliefs is by definition incorrect";

    public static void main(String[] args) {
        process(new UpCase(), s);
        process(new DownCase(), s);
        process(new Splitter(), s);
    }
}

/**
 * 该类作为基类而被扩展，用来创建各种不同类型的Processor
 */
class Processor {
    public String name() {
        return getClass().getSimpleName();
    }
    // 接受输入参数，修改它的值，然后产生输出
    Object process(Object input) {return  input;}
}

class UpCase extends Processor {
    @Override
    Object process(Object input) {
        return ((String)input).toUpperCase();
    }
}

class DownCase extends Processor {
    @Override
    Object process(Object input) {
        return ((String)input).toLowerCase();
    }
}

class Splitter extends Processor {
    @Override
    Object process(Object input) {
        return Arrays.toString(((String)input).split(" "));
    }
}
