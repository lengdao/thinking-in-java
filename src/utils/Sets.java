package utils;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Mr.Sun
 * @date 2022年02月23日 20:27
 *
 * 一个Set实用工具
 */
public class Sets {
    /**
     * 将两个参数合并在一起，即并集
     */
    public static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<T>(a);
        result.addAll(b);
        return result;
    }

    /**
     * @return 返回两个参数共有的部分，即取交集
     */
    public static <T> Set<T> intersection(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<T>(a);
        result.retainAll(b);
        return result;
    }

    /**
     * 从superset中移除subset包含的元素，既取差集
     * @return
     */
    public static <T> Set<T> difference(Set<T> superset, Set<T> subset) {
        Set<T> result = new HashSet<T>(superset);
        result.removeAll(subset);
        return result;
    }

    /**
     *
     * @return 返回储了交集之外的所有元素
     */
    public static <T> Set<T> complement(Set<T> a, Set<T> b) {
        return difference(union(a, b), intersection(a, b));
    }

}
