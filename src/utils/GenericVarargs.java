package utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年02月22日 21:12
 *
 * 可变参数与泛型方法
 */
public class GenericVarargs {

    /**
     * 该方法和标准类库中java.util.Arrays.asList()方法具有相同的功能
     * @param args
     * @param <T>
     * @return
     */
    public static <T> List<T> makeList(T... args) {
        List<T> list = new ArrayList<>();
        for (T arg : args) {
            list.add(arg);
        }
        return list;
    }

    public static void main(String[] args) {
        List<String> list = makeList("A");
        System.out.println(list);
        list = makeList("A", "B", "C");
        System.out.println(list);
        list = makeList("ABCDEFGHIGKLMNOPQRSTUVWXYZ".split(""));
        System.out.println(list);
    }

}
