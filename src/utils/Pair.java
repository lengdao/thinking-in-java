package utils;

/**
 * @author Mr.Sun
 * @date 2022年02月20日 11:29
 */
public class Pair<K, V> {
    public final K key;
    public final V value;
    public Pair(K k, V v) {
        key = k;
        value = v;
    }
}
