package utils;

/**
 * 泛型接口
 * @param <T>
 *
 * <p>
 *  生成器：专门负责创建对象的类，实际上是工厂方法设计模式的一种应用
 *  不过，当使用生成器创建新对象时，不需要任何参数，而工厂方法一般需要参数
 * </p>
 */
public interface Generator<T> {
    T next();
}
