package utils;

/**
 * @author Mr.Sun
 * @date 2022年09月02日 16:16
 */
public class OSExecuteException extends RuntimeException {
    public OSExecuteException(String why) { super(why); }
}
