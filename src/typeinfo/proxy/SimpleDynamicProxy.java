package typeinfo.proxy;

import java.lang.reflect.Proxy;

/**
 * @author Mr.Sun
 * @date 2022年02月21日 14:36
 *
 * 动态代理
 */
public class SimpleDynamicProxy {

    public static void consumer(Interface iface) {
        iface.doSomething();
        iface.somethingElse("bonobo");
    }

    public static void main(String[] args) {
        RealObject real = new RealObject();
        consumer(real);
        // Insert a proxy and call again:
        Interface proxy = (Interface) Proxy.newProxyInstance( // 通过调用静态方法Proxy.newProxyInstance()可以创建动态代理
                Interface.class.getClassLoader(), // 需要一个类加载器参数
                new Class[]{ Interface.class }, // 需要一个你希望该代理实现的接口列表（不是类或抽象类）
                new DynamicProxyHandler(real)); // 以及一个InvocationHandler接口的实现
                // 动态代理可以将所有的调用重定向到调用处理器，因此通常会向调用处理器的构造器传递一个“实际”对象的引用
                // 从而使得调用处理器在执行其中介任务时，可以将请求转发
        consumer(proxy);
    }
}
