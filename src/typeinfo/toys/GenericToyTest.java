package typeinfo.toys;

/**
 * @author Mr.Sun
 * @date 2022年02月18日 21:22
 */
public class GenericToyTest {
    public static void main(String[] args) throws Exception {
        Class<FancyToy> ftClass = FancyToy.class;
        // Produces exact type:
        FancyToy fancyToy = ftClass.newInstance();
        // 编译器只允许你声明超类引用是“某个类，他是FactoryToy的超类”，就像在
        // 表达式Class<? super FancyToy>看到的，而不会接受Class<Toy>这样的申明
        Class<? super FancyToy> up = ftClass.getSuperclass();
        // 不会通过编译
        // Class<Toy> up2 = ftClass.getSuperclass();
        // Only produces Object:
        Object obj = up.newInstance();
    }
}
