package typeinfo.factory;

/**
 * 泛型参数T使得create()可以在每种Factory实现中返回不同的类型
 * 这充分利用了协变类型
 * @param <T>
 */
public interface Factory<T> {

    T create();
}
