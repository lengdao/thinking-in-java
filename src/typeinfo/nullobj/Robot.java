package typeinfo.nullobj;

import java.util.List;

/**
 * 用接口代替具体类，用DynamicHandler创建空对象
 */
public interface Robot {
    // 一个名字
    String name();
    // 一个模型
    String model();
    // 描述robot的行为能力
    List<Operation> operations();

    // 使用嵌套类执行测试
    class Test {
        public static void test(Robot r) {
            if(r instanceof Null)
                System.out.println("[Null Robot]");

            System.out.println("Robot name: " + r.name());
            System.out.println("Robot model: " + r.model());
            for(Operation operation : r.operations()) {
                System.out.println(operation.description());
                operation.command();
            }
        }
    }
}
