package typeinfo.nullobj;

public interface Operation {
    // 一个描述
    String description();
    // 一个命令
    void command();
}
