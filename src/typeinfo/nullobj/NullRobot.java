package typeinfo.nullobj;

import java.lang.reflect.Proxy;

/**
 * @author Mr.Sun
 * @date 2022年02月21日 15:20
 *
 * 假设存在许多不同类型的Robot，我们想对每一种Robot类都创建一个空对象
 * 去执行某些特殊的操作
 */
public class NullRobot {
    public static Robot newNullRobot(Class<? extends Robot> type) {
        return (Robot) Proxy.newProxyInstance(
                NullRobot.class.getClassLoader(),
                new Class[]{ Null.class, Robot.class },
                new NullRobotProxyHandler(type));
    }

    public static void main(String[] args) {
        Robot[] bots = {
                new SnowRemovalRobot("SnowBee"),
                newNullRobot(SnowRemovalRobot.class)
        };
        for(Robot bot : bots)
            Robot.Test.test(bot);
    }
}
